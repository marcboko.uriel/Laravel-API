<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vous avez cerains de vos tâches à finir</title>
    <style>
        table, th, td {
            border:1px solid black;
          }
          .today{
              color: green
          }
          .tomorrow{
              color:blue
          }
          .late{
              color: red
          }
    </style>
</head>
<body>
    <p>  Bonjour {{ $user_tasks->identifiant }} merci pour votre confiance en nous.</p>
<div>Vous avez au total {{ count($user_tasks->tasks) }} tâche(s) à finir.Nous tenons à <br>
     vous rappeler cela afin que vous puissiez finir vos tâches en beauté.! Cordialement {{ $user_tasks->identifiant }} </div><br>
     <div>Ci-dessous le recapitulatif.</div>
     <table >
        <thead>
            <tr class="table">
                <th>Titre</th>
                <th>Description</th>
                <th>Date de fin</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user_tasks->tasks as $task)
            <tr class="table">
                <td class="table">
                    <div>
                        {{ $task->title }}
                    </div>
                </td>
                <td class="table">
                    <div >
                        {{ $task->description }}
                    </div>
                </td>
                <td class="table">
                    <div>
                        {{ $task->date_of_end }}
                        @if(date("Y-m-d",strtotime($task->date_of_end)) < date("Y-m-d"))
                        <span class="late">En retard</span>
                        @elseif (date("Y-m-d",strtotime($task->date_of_end))  === date("Y-m-d",strtotime("tomorrow")))
                        <span class="tomorrow">A finir demain</span>
                        @else
                        <span class="today">A finir dans la journée</span>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
     </table>
</body>
</html>
