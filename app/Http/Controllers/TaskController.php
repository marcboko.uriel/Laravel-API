<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use App\Exports\UsersTasksExport;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Excel as ExcelExcel;

class TaskController extends Controller
{
    /**This function create task of user authenticate
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response;
     */
    public function createTask(Request $request)
    {

        $request->validate([
            'title' => 'required|max:25|string',
            'description' => "required|string",
            'date_of_end' => "required|date_format:Y-m-d H:i:s|after_or_equal:start_date",
            'start_date' => "required|date_format:Y-m-d H:i:s|after_or_equal:today"
        ]);
        $task = Task::make([
            "title" => $request->title,
            "description" => $request->description,
            "date_of_end" => $request->date_of_end,
            "start_date" => $request->start_date
        ]);
        $task->user()->associate(Auth::user());
        $task->save();
        return response([
            'message' => 'Task create succefuly',
            'data' => $task
        ],201);
    }

    /**This function update task of user authenticate
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response;
     */
    public function updateTask(Task $task, Request $request)
    {
        $request->validate([
            'title' => 'max:25|string',
            'description' => "|string",
            'date_of_end' => "date_format:Y-m-d H:i:s|after_or_equal:start_date",
            'start_date' => "date_format:Y-m-d H:i:s|after_or_equal:today"
        ]);

        if(Auth::user()->id != $task->user_id)
        {
            return response([
                'message' => 'Unautorized, this task not found or is not for you',
            ],400);
        }
        $task->update($request->all());
        return response([
            'message' => 'Task update succefuly',
            'data' => $task
        ],200);
    }

      /**This function delete a task of user authenticate
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response;
     */
    public function deleteTask(Task $task, Request $request)
    {

        if(Auth::user()->id != $task->user_id)
        {
            return response([
                'message' => 'Unautorized, this task not found or is not for you',
            ],400);
        }
        $task->delete();
        return response([
            'message' => 'Task delete succefuly'
        ],200);
    }
     /**This function get all tasks create by user authenticate
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response;
     */
    public function getAllTasksCreateByUser(Request $request)
    {
        $tasks = Task::where('user_id',Auth::user()->id)->get();
        return response([
            'data' =>$tasks
        ]);
    }

    /**This function return all tasks create
     @param Illuminate\Http\Request
     * @return Illuminate\Http\Response*/

    public function index()
    {
        return response([
        'result' =>Task::all(),
        'message' => 'All tasks'
        ]);

    }


    /**This function update task status
     @param Illuminate\Http\Request
     * @return Illuminate\Http\Response*/

     public function updateTaskStatus(Request $request, Task $task)
     {
         $request->validate([
            'status' => 'required|integer|min:0|max:2'
         ]);

         if((Auth::user()->id != $task->user_id))
         {
             return response([
                 'message' => 'Unautorized, this task not found or is not for you',
             ],400);
         }
         $task->update($request->all());
         return response([
            'message' => 'Task status update succefuly',
            'result' => $task
        ],200);
     }

     /**This function return all tasks to do(status==0) by user
     @param Illuminate\Http\Request
     * @return Illuminate\Http\Response*/
    // if(count($tasks) == 0 )
    // {
    //     return response([
    //         'message' => 'Empty, user not get task to do',
    //     ],200);
    // }
     public function getAllTasksToDoByUser(Request $request)
     {
            $tasks = Task::where('user_id',Auth::user()->id)->where('status',0)->get();
            return response([
                'result' => $tasks
            ],200);
     }

     /**This function return all tasks doing(status==1) by user
     @param Illuminate\Http\Request
     * @return Illuminate\Http\Response*/
    public function getAllTasksDoingByUser(Request $request)
    {
           $tasks = Task::where('user_id',Auth::user()->id)->where('status',1)->get();
           return response([
               'result' => $tasks
           ],200);
    }


     /**This function return all tasks end(status==2) by user
     @param Illuminate\Http\Request
     * @return Illuminate\Http\Response*/
    public function getAllTasksEndByUser(Request $request)
    {
           $tasks = Task::where('user_id',Auth::user()->id)->where('status',2)->get();
          
           return response([
               'result' => $tasks
           ],200);
    }

    public function exportTasks()
    {
        return Excel::download(new UsersTasksExport, 'users.csv',ExcelExcel::CSV,['Content-type' => 'text/csv']);
    }
}

