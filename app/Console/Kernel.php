<?php

namespace App\Console;
use App\Mail\SendEmailBeforeTaskEnd;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     * dailyAt('08:00')
     */

    protected function schedule(Schedule $schedule)
    {
        $tomorow = Carbon::tomorrow()->format('Y-m-d');
        $date_now = Carbon::now()->format('Y-m-d');

        $users_tasks = User::has('tasks')
        ->with('tasks',function($query) use($tomorow,$date_now) {
            $query->where('status','!=',2)
             ->where(function($query) use($tomorow,$date_now) {
        $query->where(DB::raw('date(date_of_end)'),$date_now)
        ->orWhere(DB::raw('date(date_of_end)'),$tomorow)
        ->orWhereDate(DB::raw('date(date_of_end)'), '<' ,$date_now);
            });
        })->get();
        foreach ($users_tasks as $key => $value) {
            if(count([$value->tasks]) != 0)
            {
                $schedule->call(function() use($value) {
                    Mail::to($value->email)->send(new SendEmailBeforeTaskEnd($value));
                })->dailyAt("08:00");
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
